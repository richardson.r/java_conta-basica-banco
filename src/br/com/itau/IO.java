package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {
    public static void imprimirMensagemInicial() {
        System.out.println("Bem-vindo ao sistema de cadastro de conta do Bancou!");
    }

    public static Map<String, String> solicitarDadosCliente() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite o nome: ");
        String nome = scanner.nextLine();

        System.out.println("Digite o cpf: ");
        String cpf = scanner.nextLine();

        System.out.println("Digite a idade: ");
        String idade = scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);
        dados.put("cpf", cpf);
        dados.put("idade", idade);

        return dados;
    }

    public static Map<String, String> solicitarDadosConta() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite a agencia: ");
        String agencia = scanner.nextLine();

        System.out.println("Digite a conta: ");
        String conta = scanner.nextLine();


        Map<String, String> dados = new HashMap<>();
        dados.put("agencia", agencia);
        dados.put("conta", conta);

        return dados;
    }

    public static double solicitarValorReais() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o valor em R$: ");
        return scanner.nextDouble();
    }

    public static int menuOpcoes() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("O que gostaria de fazer agora: ");
        System.out.println(" (1) Ver o saldo");
        System.out.println(" (2) Depositar");
        System.out.println(" (3) Sacar");
        System.out.println(" (0) Sair");
        return scanner.nextInt();
    }
}
