package br.com.itau;

public class Cliente {
    private String nome;
    private String cpf;
    private int idade;
    private Conta conta;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public Conta getConta() {
        return conta;
    }


    public Cliente(String nome, String cpf, int idade, int agencia, int conta) {
        this.nome = nome;
        this.cpf = cpf;
        this.idade = idade;
        this.conta = new Conta(agencia, conta);
    }

    public void exibirMenu(){

        switch (IO.menuOpcoes()){
            case(1):{
                System.out.println("O saldo atual é de " + this.conta.getSaldo() + " reais. \n");
                break;
            }
            case(2):{
                if(this.conta.depositar(IO.solicitarValorReais()))
                    System.out.println("Valor depositado com sucesso.\n");
                else
                    System.out.println("Não foi possível dopositar o valor inserido.\n");
                break;
            }
            case(3):{
                if(this.conta.sacar(IO.solicitarValorReais()))
                    System.out.println("Valor sacado com sucesso.\n");
                else
                    System.out.println("Não foi possível fazer o saque do valor inserido.\n");
                break;
            }
            case(0):{
                System.out.println("Obrigado por utilizar o Bancou.\n");
                System.out.println("Tenha um bom dia!!!");
                return;
            }
            default:
                break;
        }
        exibirMenu();
    }
}
