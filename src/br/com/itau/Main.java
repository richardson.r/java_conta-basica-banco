package br.com.itau;

import java.util.Map;

public class Main {

    public static void main(String[] args) {
        IO.imprimirMensagemInicial();

        //Solicita dados do cliente
        Map<String, String> dadosCliente = IO.solicitarDadosCliente();

        //Valida idade do cliente
        if (Integer.parseInt(dadosCliente.get("idade")) < 18) {
            System.out.println("Cliente precisa ser maior de 18 anos para criar a conta.");
            return;
        }

        //Solicita dados da conta
        Map<String, String> dadosConta = IO.solicitarDadosConta();

        //Cadastra cliente
        Cliente cliente = new Cliente(
                dadosCliente.get("nome"),
                dadosCliente.get("cpf"),
                Integer.parseInt(dadosCliente.get("idade")),
                Integer.parseInt(dadosConta.get("agencia")),
                Integer.parseInt(dadosConta.get("conta"))
        );

        System.out.println("Conta criada com sucesso!\n");

        //Exibe menu de opções do cliente
        cliente.exibirMenu();
    }


}
