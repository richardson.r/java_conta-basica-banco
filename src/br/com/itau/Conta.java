package br.com.itau;

public class Conta {
    private int agencia;
    private int conta;
    private double saldo;

    public int getAgencia() {
        return agencia;
    }
    public int getConta() {
        return conta;
    }
    public double getSaldo() {
        return saldo;
    }

    public Conta(int agencia, int conta){
        this.agencia = agencia;
        this.conta = conta;
        this.saldo = 0.0;
    }

    public boolean depositar(double valor){
        if(valor > 0) {
            this.saldo = this.saldo + valor;
            return true;
        }
        else
            return false;
    }

    public boolean sacar(double valor){
        if(valor > 0 && valor <= this.saldo) {
            this.saldo = this.saldo - valor;
            return true;
        }
        else
            return false;
    }


}
